
#include "BoundingBox.h"

BoundingBox::BoundingBox(float xpos, float ypos, float width, float height)
	: xpos(xpos), ypos(ypos), width(width), height(height)
{};

bool BoundingBox::isPointInside(float px, float py)
{
	if (px >= xpos && px < xpos + width)
	{
		if (py >= ypos && ypos < ypos + height)
		{
			return true;
		}
	}

	return false;
}