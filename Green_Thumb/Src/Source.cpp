
/**

	TODO:
		- Make some kind of input handler for mouse events. We could make a mousePressed() and mouseReleased() like processing pretty easily.
		- Implement torque and translational force physics in Plant_Node
		- Set up text input handler thing from windows
			- Use it to write input to the console screen
			- Parse valid inputs in Console class

	TOUGHTS:
		I am concerned about having only static enemies. Combat won't really work without needing to use the movement system along side it. 
		Perhaps there should be more interaction between spells and animals, or perhaps there should be hostile animals. I want ways for the systems to be integrated.
		I think movement and spells and animals might be able to work together somehow.


		It seems like there is a lot of code in here that isn't really reliable. It doesn't work exactly in the way that you'd expect it to and the way the physics system is set up is not really intuitive.
		Now as we try to advance the system we suffer from small bugs. I want to have a better way of keeping track of the system.

**/









#include "SDL.h"
#include "SDL_rect.h"
#include "SDL_render.h"
#include "Game.h"
#include <iostream>
#include <cstdlib>


using namespace std;

Game *game = nullptr;

int main(int argc, char* argv[])
{
	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frameStart;
	int frameTime;

	game = new Game();
	game->init("Green Thumb", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1600, 900, false);

	while (game->running())
	{

		frameStart = SDL_GetTicks();	// Find the tick when the frame started processing

		// Do the core frame processing
		{
			game->handleEvents();
			game->update();
			game->render();
		}

		frameTime = SDL_GetTicks() - frameStart;	// Find how many ticks it took to do the core processing

		if (frameTime < frameDelay)	// If it took less time than it should have, delay for the difference.
		{
			SDL_Delay(frameDelay - frameTime);
		}
	}

	game->clean();

	system("pause");
	return 0;
}

