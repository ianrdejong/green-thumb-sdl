

#include "Vector2D.h"

class PhysicsComponent {

public:
	PhysicsComponent();
	~PhysicsComponent();

	void setMass(unsigned int mass);
	void setPosition(Vector2D position);
	void setVelocity(Vector2D velocity);
	void setAcceleration(Vector2D acceleration);
	
	unsigned int getMass() const;
	Vector2D getPosition() const;
	Vector2D getVelocity() const;
	Vector2D getAcceleration() const;

public:
	void update();
	void applyConstantForce(Vector2D force);	// Apply a force that will remain
	void applyImpulse(Vector2D force);	// Apply a force that lasts for a single update cycle

private:
	double mass;
	Vector2D position;
	Vector2D velocity;
	Vector2D acceleration;


};