#pragma once

#include "SDL.h"
#include "Colour_Constants.h"

class Vector2D{
public:
	Vector2D();
	~Vector2D();

	void setX(float x);
	void setY(float y);
	void setPosition(SDL_Point position);
	void setAngleFromVertical(double dir);
	void setAngleFromHorizontal(double dir);
	void setMag(float mag);
	void addMag(float mag);

	const float getX();
	const float getY();
	const double getAngleFromHorizontal();
	const double getAngleFromVertical();
	const float getMag();
	const SDL_Point getPosition();


	void addAngle(double angle);		// Adjust this vector's angle by some amount
	void subtractAngle(double angle);	
	double getAngleSum(double a1);	// Add two angles together and return the normalized result
	double getAngleDifference(double a1);
	void normalizeAngle();	// Bring the angle into normal range {0 - TWO_PI}
	double normalizeAngle(double angle);	// Bring an angle into normal range {0 - TWO_PI} and return it
	void invert();	

	Vector2D getComponentParallelTo(Vector2D v);
	Vector2D getComponentPerpendicularTo(Vector2D v);
	Vector2D getInverse();// Return the vector in the opposite direction

	void pointUp();		// sets the vector to point upward
	void pointDown();	// sets the vector to point downward

	Vector2D add(Vector2D);
	Vector2D subtract(Vector2D);
	Vector2D multiply(Vector2D);
	Vector2D divide(float);

	Vector2D operator+(Vector2D);
	Vector2D operator-(Vector2D);
	Vector2D operator*(Vector2D);
	Vector2D operator/(float);
	void operator+=(Vector2D);
	void operator=(Vector2D);

private:
	float x;
	float y;
	double direction;	// In radians from 0 - PI ( From horizontal ). The quadrants are layed out relative to the viewer, not the screen coords (Quadrant I is the top right)
	float magnitude;
	const double HALF_PI = M_PI / 2;
	const double THREE_HALFS_PI = 3 * (M_PI / 2);
	const double TWO_PI = M_PI * 2;

private:
	void UTIL_updateRotAndMag();	// util func that updates the rotation and magnitude based on the x and y
	void UTIL_updateXAndY();		// util func that updates the x and y based on magnitude and rotation
	double UTIL_angleToVerticalReference(double angle);	// takes an angle represented as the positive difference from horizontal and converts it to the signed difference from vertical
	double UTIL_angleToHorizontalReference(double angle);	// takes an angle represented as the signed difference from vertical and converts it to the positive difference from horizontal

public:
	void DBG_Draw(SDL_Renderer * renderer, SDL_Color color, int xBegin, int yBegin);
	void DBG_Print(const char* tag);
};