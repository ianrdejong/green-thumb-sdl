#pragma once

#include "SDL.h"
#include "SDL_rect.h"
#include "SDL_render.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "Player.h"
#include "Plant.h"
#include "Console.h"
#include "World.h"
#include "Colour_Constants.h"


class Game {

public:
	Game();
	~Game();

	void init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);

	void handleEvents();
	void update();
	void render();
	void clean();

	bool running() { return isRunning; }

private:
	SDL_Window *window;
	SDL_Renderer *renderer;
	World* world;
	Player *player;
	Console* console;
	enum Scene {CONSOLE, MAIN};
	Scene currentScene;

	bool isRunning = false;
	bool simulatePhysics = true;

	int mouseX;
	int mouseY;
	bool mouseLeftIsPressed;

	bool isLeftPressed;
	bool isRightPressed;
	bool isForwardPressed;
	bool isBackwardPressed;
};