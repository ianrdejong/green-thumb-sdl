#pragma once

class BoundingBox{

public:
	BoundingBox(float xpos, float ypos, float width, float height);

	bool isPointInside(float px, float py);


private:
	float xpos, ypos;
	float width, height;
};