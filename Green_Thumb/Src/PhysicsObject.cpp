
#include "Colour_Constants.h"
#include "PhysicsObject.h"

PhysicsObject::PhysicsObject()
{
	PhysicsComponent();

	boundary = SDL_Rect();
	boundary.x = getPosition().getX() -5;
	boundary.y = getPosition().getY() -5;
	boundary.w = 10;
	boundary.h = 10;

	color = C_BLACK;
}


void PhysicsObject::render(SDL_Renderer * renderer)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawRect(renderer, &boundary);
}