
#include "Plant.h"

Plant::Plant(int xpos, int ypos, int width, int height, SDL_Texture* texture, unsigned int txtwidth, unsigned int txtheight)
	: xpos(xpos), ypos(ypos), texture(texture)
{
	txtSrcrect.x = 0;
	txtSrcrect.y = 0;
	txtSrcrect.w = txtwidth;
	txtSrcrect.h = txtheight;

	touching.x = xpos;
	touching.y = ypos;
	touching.w = width;
	touching.h = height;
};


Plant::~Plant()
{
	SDL_DestroyTexture(texture);
}


void Plant::render(SDL_Renderer* renderer)
{
	SDL_RenderCopy(renderer, texture, &txtSrcrect, &touching);
}

void Plant::tick()
{
}

bool Plant::pointIsTouching(SDL_Point point)
{
	if (SDL_PointInRect(&point, &touching))
	{
		isAlive = false;
		return true;
	}

	return false;
}

bool Plant::checkPlayerIntersection(Player* player)
{
	if (SDL_HasIntersection(&player->getHitBox(), &touching))
	{
		isAlive = false;
		return true;
	}

	return false;
}