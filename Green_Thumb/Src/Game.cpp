#include <iostream>
#include "Game.h"

#include "Vector2D.h"

Game::Game()
{

}

Game::~Game()
{

}


// Set up the game
void Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
	int flags = 0;
	if (fullscreen)
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}

	// TODO: Make some kind of checklist logic so that if any one of these inits fails there will be an error message and isRunning will NOT be true.
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		if (TTF_Init() != -1)
		{
			std::cout << "Subsystems initialized." << std::endl;
			window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);	// Create window
			if (window)
			{
				std::cout << "Window created." << std::endl;
			}

			renderer = SDL_CreateRenderer(window, -1, 0);	// Create renderer
			if (renderer)
			{
				std::cout << "Renderer created." << std::endl;
				SDL_SetRenderDrawColor(renderer, C_GRASS.r, C_GRASS.g, C_GRASS.b, C_GRASS.a);
			}
		
			console = new Console();
			world = new World();
			world->generateWorld(renderer);
			currentScene = CONSOLE;
			isRunning = true;
		}
	}


	isLeftPressed = false;
	isRightPressed = false;
	isForwardPressed = false;
	isBackwardPressed = false;


	// SET UP PLAYER TEXTURE
	SDL_Surface* tmpSurface = IMG_Load("../Assets/pl_image.png");
	if (tmpSurface == NULL)
	{
		std::cout << "Error loading image" << std::endl;
	}

	player = new Player(100, 100, 32, SDL_CreateTextureFromSurface(renderer, tmpSurface));

	SDL_FreeSurface(tmpSurface);

}


int counter = 0;

// Handle events
void Game::handleEvents()
{
	//MOUSE
	Uint32 mouseBitMask = SDL_GetMouseState(&mouseX, &mouseY);
	if (mouseBitMask & SDL_BUTTON(SDL_BUTTON_LEFT))
	{
		int TOLERANCE = 5;
		if (!mouseLeftIsPressed)	// MOUSE LEFT FIRED
		{
			std::cout << "pressed lmb" << std::endl;
			SDL_Point mousePosition;
			mousePosition.x = mouseX;
			mousePosition.y = mouseY;

			//world->addPlant(mouseX, mouseY, renderer);
		}
		mouseLeftIsPressed = true;
	}
	else
	{
		if (mouseLeftIsPressed)	// MOUSE LEFT RELEASED
		{

		}
		mouseLeftIsPressed = false;
	}

	// KEYBOARD
	SDL_Event event;
	SDL_PollEvent(&event);
	switch (event.type)
	{
	case SDL_QUIT:		// QUIT GAME (break running loop)
		isRunning = false;
		break;

	case SDL_KEYDOWN:	// KEY PRESSED
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_a:
				isLeftPressed = true;
				break;
					
			case SDLK_d:
				isRightPressed = true;
				break;

			case SDLK_w:
				isForwardPressed = true;
				break;

			case SDLK_s:
				isBackwardPressed = true;
				break;

			case SDLK_c:
				if (currentScene == CONSOLE)
				{
					currentScene = MAIN;
				}
				else if (currentScene == MAIN)
				{
					currentScene = CONSOLE;
				}
				break;

			case SDLK_f:
				world->addPlantCell();
				break;

			case SDLK_q:
				break;

			case SDLK_e:
				break;

			case SDLK_SPACE:
				simulatePhysics = !simulatePhysics;
				break;

			default:
				std::cout << "key not recognized" << std::endl;
				break;
			}
		}
		break;

	case SDL_KEYUP:	// KEY RELEASED
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_a:
				isLeftPressed = false;
				break;

			case SDLK_d:
				isRightPressed = false;
				break;

			case SDLK_w:
				isForwardPressed = false;
				break;

			case SDLK_s:
				isBackwardPressed = false;
				break;

			default:
				break;
			}
		}
		break;


	default:
		break;
	}
}


// Update everything in the game
void Game::update()
{
	if (currentScene == MAIN)
	{
		if (isLeftPressed)	// This logic should probably go in the Player class
		{
			player->moveLeft();
		}

		if (isRightPressed)
		{
			player->moveRight();
		}

		if (isForwardPressed)
		{
			player->moveUp();
		}

		if (isBackwardPressed)
		{
			player->moveDown();
		}

		player->updatePosition();
		world->updatePlayer(player);
		if (simulatePhysics)
		{
			world->update(renderer);
		}
	}
	
	else if (currentScene == CONSOLE)
	{

	}
}


// Render everything in the game
void Game::render()
{
	if (currentScene == MAIN)	// RENDER MAIN GAME
	{
		world->render(renderer);
		player->render(renderer);
	}

	else if (currentScene == CONSOLE)	// RENDER CONSOLE
	{
		console->render(renderer);
	}

	SDL_RenderPresent(renderer);
}


// Destroy things when shutting down
void Game::clean()
{
	// TODO: Free pointers to plants, player, etc.
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	world->~World();
	player->~Player();
	console->~Console();
	TTF_Quit();
	SDL_Quit();

	std::cout << "Game cleaned up." << std::endl;
}