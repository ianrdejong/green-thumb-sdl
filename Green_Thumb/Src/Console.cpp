#include "Console.h"

Console::Console()
{
	inputHistory = std::vector<char*>();
	currentInput = std::vector<char>();
	textColor = {255, 255, 255, 255};
	font = TTF_OpenFont("cour.ttf", 50);
}


Console::~Console()
{
	TTF_CloseFont(font);
	SDL_FreeSurface(textSurface);
	SDL_DestroyTexture(textTexture);
}


void Console::inputKey(SDL_Keycode key)
{
	if (key == SDLK_RETURN)	// IF KEY == ENTER
	{
		char* finishedLine = new char[currentInput.size()];	// Create a char* on the heap to store the completed line
		for (size_t i = 0; i < currentInput.size(); i++)	// Assign the input vector to the completed line, element by element
		{
			finishedLine[i] = currentInput.at(i);
		}
		inputHistory.push_back(finishedLine);	// Add the char* to the inputHistory vector
	}
	else
	{	// IF KEY != ENTER
		//currentInput.push_back(SDL_GetKeyName(key));
	}
}


void Console::render(SDL_Renderer* renderer)
{
	SDL_SetRenderDrawColor(renderer, C_BLACK.r, C_BLACK.g, C_BLACK.b, C_BLACK.a);
	textSurface = TTF_RenderText_Solid(font, "test", textColor);
	textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_RenderCopy(renderer, textTexture, NULL, NULL);
}