
#include <iostream>
#include "Vector2D.h"




/////////////////////////////////
/******* CTORS AND DTORS *******/

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
	magnitude = 0;
	direction = 0;
}


Vector2D::~Vector2D()
{

}


 




/////////////////////////////////////
/******* SETTERS AND GETTERS *******/

void Vector2D::setX(float x)
{
	this->x = x;
	UTIL_updateRotAndMag();
}


void Vector2D::setY(float y)
{
	this->y = y;
	UTIL_updateRotAndMag();
}


void Vector2D::setPosition(SDL_Point position)
{
	this->x = position.x;
	this->y = position.y;
	UTIL_updateRotAndMag();
}


void Vector2D::setAngleFromHorizontal(double dir)
{
	this->direction = dir;
	normalizeAngle();
	UTIL_updateXAndY();
}


void Vector2D::setAngleFromVertical(double dir)
{
	this->direction = UTIL_angleToHorizontalReference(dir);
	normalizeAngle();
	UTIL_updateXAndY();
}


void Vector2D::setMag(float mag)
{
	this->magnitude = mag;
	UTIL_updateXAndY();
}


void Vector2D::addMag(float mag)
{
	this->magnitude += mag;
}


const float Vector2D::getX()
{
	return x;
}


const float Vector2D::getY()
{
	return y;
}


const double Vector2D::getAngleFromHorizontal()
{
	return direction;
}


const double Vector2D::getAngleFromVertical()
{
	return UTIL_angleToVerticalReference(direction);
}


const float Vector2D::getMag()
{
	return magnitude;
}



const SDL_Point Vector2D::getPosition()
{
	SDL_Point point = { (int)x, (int)y };
	return point;
}






////////////////////////////////////
/******* BASIC MANIPULATION *******/


Vector2D Vector2D::getInverse()
{
	Vector2D inverse = *this;
	inverse.addAngle(M_PI);
	return inverse;
}


void Vector2D::invert()
{
	this->addAngle(M_PI);
}


void Vector2D::addAngle(double angle)
{
	direction += angle;
	normalizeAngle();
	UTIL_updateXAndY();
}


double Vector2D::getAngleSum(double angle)
{
	double sum = this->getAngleFromHorizontal() + angle;
	sum = normalizeAngle(sum);
	return sum;
}


void Vector2D::subtractAngle(double angle)
{
	direction -= angle;
	normalizeAngle();
	UTIL_updateXAndY();
}


double Vector2D::getAngleDifference(double angle)
{
	double difference = this->getAngleFromHorizontal() - angle;
	difference = normalizeAngle(difference);
	return difference;
}


void Vector2D::pointUp()
{
	direction = HALF_PI;
	UTIL_updateXAndY();
}

void Vector2D::pointDown()
{
	direction = THREE_HALFS_PI;
	UTIL_updateXAndY();
}


// Bring the angle into range 0 - TWO_PI
void Vector2D::normalizeAngle()
{
	while (direction >= TWO_PI)
	{
		direction -= TWO_PI;
	}
	while (direction < 0)
	{
		direction += TWO_PI;
	}
}


double Vector2D::normalizeAngle(double angle)
{
	while (angle >= TWO_PI)
	{
		angle -= TWO_PI;
	}
	while (angle < 0)
	{
		angle += TWO_PI;
	}

	return angle;
}







/////////////////////////////
/******* VECTOR MATH *******/


Vector2D Vector2D::add(Vector2D v)
{
	Vector2D sum = *this;
	sum.setX(sum.getX() + v.getX());
	sum.setY(sum.getY() + v.getY());
	return sum;
}


Vector2D Vector2D::subtract(Vector2D v)
{
	Vector2D difference = *this;
	difference.setX(difference.getX() - v.getX());
	difference.setY(difference.getY() - v.getY());
	return difference;
}


Vector2D Vector2D::multiply(Vector2D v)
{
	UTIL_updateRotAndMag(); // TODO
	return *this;
}


Vector2D Vector2D::divide(float amount)
{
	Vector2D quotient = *this;
	quotient.setMag(quotient.getMag() / amount);
	return quotient;
}


Vector2D Vector2D::operator+(Vector2D v)
{
	
	return add(v);
}


Vector2D Vector2D::operator-(Vector2D v)
{
	return subtract(v);
}


Vector2D Vector2D::operator*(Vector2D v)
{
	multiply(v);
	return *this;
}


Vector2D Vector2D::operator/(float amount)
{
	return divide(amount);
}


void Vector2D::operator+=(Vector2D v)
{
	*this = add(v);
}


void Vector2D::operator=(Vector2D v)
{
	x = v.getX();
	y = v.getY();
	direction = v.getAngleFromHorizontal();
	magnitude = v.getMag();
}







////////////////////////////
/******* COMPONENTS *******/


Vector2D Vector2D::getComponentPerpendicularTo(Vector2D v)
{
	Vector2D perpendicular;

	double angleDifference = getAngleDifference(v.getAngleFromHorizontal());	// Find the difference in angle between the two vectors
	perpendicular.setAngleFromHorizontal(v.getAngleFromHorizontal());	// Initialize perpendicular angle to be the SAME as the input angle. (This will be updated in a few lines)
	perpendicular.setMag(abs(this->getMag() * (float)sin(angleDifference)));
	
	if (angleDifference >= 0 && angleDifference < M_PI)			// Quadrant I and II
	{
		perpendicular.addAngle(HALF_PI);
	}
	else if (angleDifference >= M_PI && angleDifference <= TWO_PI)	// Quadrant III and IV
	{
		perpendicular.subtractAngle(HALF_PI);
	}

	getComponentParallelTo(v);
	return perpendicular;
}


Vector2D Vector2D::getComponentParallelTo(Vector2D v)
{
	Vector2D parallelComponent;

	parallelComponent.setAngleFromHorizontal(v.getAngleFromHorizontal());	// The directions are the SAME
	double angleDifference = getAngleDifference(v.getAngleFromHorizontal());	// Get the positive difference between angles
	float parallelMagnitude = this->getMag() * (float)cos(angleDifference);

	if (parallelMagnitude >= 0)
	{
		parallelComponent.setMag(parallelMagnitude);
	}
	else
	{
		parallelComponent.setMag(-parallelMagnitude);
		parallelComponent.invert();
	}

	return parallelComponent;
}






/////////////////////////
/******* UTILITY *******/


void Vector2D::UTIL_updateXAndY()
{
	x = magnitude * (float)cos(direction);
	y = -(magnitude * (float)sin(direction));
}


void Vector2D::UTIL_updateRotAndMag()
{
	if (x > 0 && y <= 0)			// Quadrant I
	{
		direction = atan(-y / x);
	}
	else if (x < 0 && y <= 0)		// Quadrant II
	{
		direction = M_PI + atan(-y / x);
	}
	else if (x < 0 && y >= 0)		// Quadrant III
	{
		direction = M_PI + atan(-y / x);
	}
	else if (x > 0 && y >= 0)		// Quadrant IV
	{
		direction = TWO_PI + atan(-y / x);
	}
	else if (x == 0 && y <= 0)		// Vertical upwards
	{
		direction = HALF_PI;
	}
	else if (x == 0 && y > 0)		// Vertical downwards
	{
		direction = THREE_HALFS_PI;
	}

	if ((float)sin(direction) != 0 && y != 0)	// Set magnitude, avoid asympototes
	{
		magnitude = abs(y / (float)sin(direction));
	}
	else
	{
		magnitude = abs(x / (float)cos(direction));
	}
}



double Vector2D::UTIL_angleToVerticalReference(double angle)
{
	angle = normalizeAngle(angle);

	if (angle >= 0 && angle < HALF_PI)	// Quadrant 1
	{
		return (HALF_PI - angle);
	}

	else if (angle >= HALF_PI && angle < THREE_HALFS_PI)	// Quadrant II or III
	{
		return HALF_PI - angle;
	}

	else if (angle >= THREE_HALFS_PI && angle < TWO_PI)	// Quadrant IV
	{
		return M_PI - (angle - THREE_HALFS_PI);
	}

	else
	{
		return -9999;	// Return flag value if input could not be processed
	}
}


double Vector2D::UTIL_angleToHorizontalReference(double angle)	// ehhh, does this make sense???
{
	if (angle < 0)		// Quadrants II or III or lower
	{
		return -(angle - M_PI / 2);
	}

	else if (angle >= 0 && angle < M_PI / 2)	// Quadrant I
	{
		return M_PI /2 - angle;
	}

	else if (angle >= M_PI / 2)	// Quadrant IV or higher
	{
		return -(angle - M_PI / 2);
	}
	
	else
	{
		return -9999;
	}
}






///////////////////////
/******* DEBUG *******/


void Vector2D::DBG_Draw(SDL_Renderer * renderer, SDL_Color color, int xBegin, int yBegin)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawLine(renderer, xBegin, yBegin, (int)x + xBegin, (int)y + yBegin);
	SDL_Rect tip;
	tip.x = (int)x + xBegin;
	tip.y = (int)y + yBegin;
	tip.w = 3;
	tip.h = 3;
	SDL_RenderDrawRect(renderer, &tip);
}


void Vector2D::DBG_Print(const char* tag)
{
	std::cout << "Vector " << tag << ": " << std::endl;
	std::cout << "x: " << x << ", y: " << y << std::endl;
	std::cout << "mag: " << magnitude << ", angleFromHorizontal: " << getAngleFromHorizontal() << ", angleFromVertical: " << getAngleFromVertical() << std::endl;
	std::cout << "\n" << std::endl;
}