#pragma once

#include "Player.h"
#include "SDL.h"

unsigned int const INTERACTIVE_DISTANCE = 5;

// Eventually the plant class may just be a wrapper for a Plant_Node tree.
class Plant {

public:
	Plant(int xpos, int ypos, int width, int height, SDL_Texture* texture, unsigned int txtwidth, unsigned int txtheight);
	~Plant();

	void render(SDL_Renderer* renderer);
	bool pointIsTouching(SDL_Point point);
	bool checkPlayerIntersection(Player* player);	// *** This should not take a player but a rect.
	void tick();
	bool getIsAlive() { return isAlive; }

private:
	bool isAlive = true;
	int xpos;
	int ypos;
	SDL_Texture* texture;
	SDL_Rect txtSrcrect;
	SDL_Rect touching;
};