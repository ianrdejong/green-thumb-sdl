#pragma once

#include <vector>
#include "Colour_Constants.h"
#include "SDL.h"
#include "SDL_ttf.h"

class Console{

public:
	Console();
	~Console();

	void render(SDL_Renderer* renderer);
	void inputKey(SDL_Keycode key);

private:
	std::vector<char*> inputHistory;
	std::vector<char> currentInput;
	TTF_Font* font;
	SDL_Color textColor;
	SDL_Surface* textSurface;
	SDL_Texture* textTexture;
};