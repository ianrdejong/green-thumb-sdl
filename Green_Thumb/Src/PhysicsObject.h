
#include "PhysicsComponent.h"
#include "SDL.h"

class PhysicsObject : public PhysicsComponent{

public:
	PhysicsObject();
	~PhysicsObject();

	void render(SDL_Renderer * renderer);

private:
	SDL_Rect boundary;
	SDL_Color color;
};