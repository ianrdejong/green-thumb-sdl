
#include <iostream>

#include "Plant_Node.h"
#include "SDL_ttf.h"




/* -------------- CTORS & DTOR -------------- */

Plant_Node::Plant_Node()
{
	position = {0, 0};
	parent = nullptr;
	children = std::vector<Plant_Node*>();

	positionRelativeToParent = Vector2D();			// init position and original position
	originalPositionRelativeToParent = Vector2D();

	originalPositionRelativeToParent.setAngleFromVertical((float)(std::rand() % 200) / 100 - 1); // Generate a float between -1 and 1
	originalPositionRelativeToParent.setMag(DEFAULT_DISTANCE);		// Use the default distance between nodes as the magnitude
	positionRelativeToParent = originalPositionRelativeToParent;	// Initialize the position as the original position relative to parent

	mass = DEFAULT_MASS;
}

Plant_Node::Plant_Node(SDL_Point pos) : Plant_Node()
{
	position = pos;
}

Plant_Node::~Plant_Node()
{
	for (size_t i = 0; i < children.size(); i++)
	{
		delete children.at(i);
		children.at(i) = nullptr;
	}
}




/* -------------- PHYSICS FUNCTIONS -------------- */


/*
	Master function for calculation of forces
*/
void Plant_Node::calculatePhysics(SDL_Renderer * renderer)
{
	calculateAppliedForces(renderer);
	calculateNormalForces(renderer);
	calculateNetForces(renderer);
}




/*	
	Recursively move towards the root and calculate the total force applied to each node. 
	Since the forces transferred between nodes are always parallel to the axis of the edges the applied force must be broken down into parallel and angular components.
*/	
void Plant_Node::calculateAppliedForces(SDL_Renderer * renderer)
{
	// STEP 1: Set up the gravitational force vector applied to this node.
	Vector2D gravitationalForce;
	gravitationalForce.setMag(GRAVITATIONAL_CONSTANT * mass);
	gravitationalForce.pointDown();


	// STEP 2: Apply the gravitational force
	appliedForce = gravitationalForce;


	// STEP 3: If this node has children then recursively call because forces are passed on from children to parents (since plants only grow up?)
	if (!children.empty())
	{
		children.front()->calculateAppliedForces(renderer);	// We need to know the applied forces on the children first so recursive call
		appliedForce += children.front()->parallelForceApplied;
	}


	if (parent != nullptr)
	{
		// STEP 4: Split the applied force into angular and parallel components
		angularForceApplied = appliedForce.getComponentPerpendicularTo(positionRelativeToParent);
		parallelForceApplied = appliedForce.getComponentParallelTo(positionRelativeToParent);


		// STEP 5: Draw the forces
		angularForceApplied.DBG_Draw(renderer, C_BLUE, position.x, position.y);	// Draw applied force vectors
		parallelForceApplied.DBG_Draw(renderer, C_BLUE, position.x, position.y);
	}

	appliedForce.DBG_Draw(renderer, C_RED, position.x, position.y);	// Draw the applied force

	return;
}




/*
	Recursively move toward the leaves and calculate all normal forces
*/
void Plant_Node::calculateNormalForces(SDL_Renderer * renderer) 
{
	// STEP 1: Calculate the angular normal force
	if (parent != nullptr)	// If NOT root node
	{
		angularForceNormal = angularForceApplied.getInverse();	// Set direction of angular normal
		angularForceNormal.setMag(abs(getAngularDisplacement() * 100));	// Set the mag of the angular normal as a function of the angular disp
		if (angularForceNormal.getMag() > angularForceApplied.getMag())	// Cap the mag of the angular normal force
		{
			angularForceNormal.setMag(angularForceApplied.getMag());
		}
		angularForceNormal.DBG_Draw(renderer, C_LIGHT_BLUE, position.x, position.y);	// Draw
	}



	// STEP 2: Calculate the parallel normal force
	if (parent != nullptr)	// The root node gets no normals
	{
		if (parent->parent == nullptr)	// If parent is root node then paralell normal is the opposite of what was applied
		{
			parallelForceNormal = parallelForceApplied.getInverse();
		}
		else
		{		// For most nodes the parallel normal is the component parallel to the positionRelativeToParent of the sum of the parents' normal components
			parallelForceNormal = (parent->angularForceNormal + parent->parallelForceNormal).getComponentParallelTo(positionRelativeToParent);
		}

		if (parallelForceNormal.getMag() > parallelForceApplied.getMag())	// Cap the mag of the parallel normal force
		{
			parallelForceNormal.setMag(parallelForceApplied.getMag());
		}
		parallelForceNormal.DBG_Draw(renderer, C_LIGHT_BLUE, position.x, position.y);
	}



	// STEP 3: Recursive call
	if (!children.empty())
	{
		children.front()->calculateNormalForces(renderer);
	}

	return;
}





void Plant_Node::calculateNetForces(SDL_Renderer * renderer)
{
	// STEP 1: Calculate net angular
	angularForceNet = angularForceApplied + angularForceNormal;
	angularForceNet.DBG_Draw(renderer, C_DARK_RED, position.x, position.y);	// Draw angular

	// STEP 2: Calculate net translational
	parallelForceNet = parallelForceApplied + parallelForceNormal;
	parallelForceNet.DBG_Draw(renderer, C_DARK_RED, position.x, position.y);	// Draw translational


	// STEP 3: Recursive call
	if (!children.empty())
	{
		children.front()->calculateNetForces(renderer);
	}
}



double Plant_Node::getAngularDisplacement()
{
	if (parent != nullptr)
	{
		return positionRelativeToParent.getAngleFromVertical() - originalPositionRelativeToParent.getAngleFromVertical();
	}

	return 0.0;
}



void Plant_Node::applyPhysics()
{
	// Update position
	//positionRelativeToParent.setMag(positionRelativeToParent.getMag() + parallelForceNet.getMag() * 0.001);

	// Update rotation
	if (parent != nullptr)
	{
		float angleToAdd = angularForceNet.getMag() * 0.0001;
		if (angularForceNet.getAngleFromVertical() > 0)
		{
			angleToAdd = -angleToAdd;
		}
		positionRelativeToParent.addAngle(angleToAdd);
	}

	updatePosition();

	if (!children.empty())
	{
		children.front()->applyPhysics();
	}
}





/* -------------- OTHER FUNCTIONS -------------- */


void Plant_Node::tick(SDL_Renderer * renderer)
{
	calculatePhysics(renderer);
	//applyPhysics();
	render(renderer);
}



// This function should have the option to add a second child instead of always replacing
void Plant_Node::addChild()
{
	Plant_Node * child = new Plant_Node();
	child->setParent(this);

	if (!children.empty())
	{
		Plant_Node * tmp = children.front();
		tmp->setParent(child);
		child->children.push_back(tmp);
		this->children.front() = child;	// Assign our new child if the vector wasn't empty
	}
	else
	{
		this->children.push_back(child);	// Assign our new child if the vector was empty
	}

	child->updatePosition();

	std::cout << std::endl;
}



// Recursively render all daughter nodes
void Plant_Node::render(SDL_Renderer * renderer)
{
	SDL_Rect target;	// Create a rectangle with which to represent this node
	target.x = position.x - 5;
	target.y = position.y - 5;
	target.w = 10;
	target.h = 10;

	SDL_SetRenderDrawColor(renderer, C_PLANTS.r, C_PLANTS.g, C_PLANTS.b, C_PLANTS.a);	// Render filled rects
	SDL_RenderFillRect(renderer, &target);	// Draw this node as the target rect

	SDL_SetRenderDrawColor(renderer, C_PLANTS_DARK.r, C_PLANTS_DARK.g, C_PLANTS_DARK.b, C_PLANTS_DARK.a);	// Render outlines
	SDL_RenderDrawRect(renderer, &target);


	for (size_t i = 0; i < children.size(); i++) // Render all children
	{
		children.at(i)->render(renderer);
	}

	if (parent != nullptr)
	{
		positionRelativeToParent.DBG_Draw(renderer, C_PLANTS_DARK, parent->position.x, parent->position.y);
	}
}



void Plant_Node::updatePosition()	// Untested. Recursive to all daughters.
{
	if (parent == nullptr)	// Base case
	{
		return;
	}

	int xOffset = (int)positionRelativeToParent.getX();
	int yOffset = (int)positionRelativeToParent.getY();

	position.x = parent->getPosition().x + xOffset;
	position.y = parent->getPosition().y + yOffset;

	if (!children.empty())
	{
		for (Plant_Node * node : children)	// Recursive step
		{
			node->updatePosition();
		}
	}
}



Plant_Node * Plant_Node::findNodeOnPoint(SDL_Point point)
{
	SDL_Rect target;	// Create a rectangle with which to represent this node
	target.x = position.x;
	target.y = position.y;
	target.w = 10;
	target.h = 10;

	if (SDL_PointInRect(&point, &target))
	{
		return this;
	}
	else 
	{
		if (!children.empty())
		{
			return children.front()->findNodeOnPoint(point);
		}

	}

	return nullptr;
}







/* -------------- SETTERS & GETTERS -------------- */

void Plant_Node::setPosition(const SDL_Point * point)
{
	this->position = *point;
}


SDL_Point Plant_Node::getPosition()
{
	return this->position;
}


void Plant_Node::setParent(Plant_Node * parent)
{
	this->parent = parent;
}





/* -------------- DEBUG -------------- */

void Plant_Node::DBG_PrintSingle()
{
	std::cout << "Plant_Node -----------------" << std::endl;
	std::cout << "Position: (" << position.x << "," << position.y << ")" << std::endl;

	std::cout << "Has Parent: ";	// Has parent?
	if (parent != nullptr)
	{
		std::cout << "True" << std::endl;
	}
	else
	{
		std::cout << "False" << std::endl;
	}

	std::cout << "Angle From Parent: " << positionRelativeToParent.getAngleFromVertical() << std::endl;
	std::cout << "Has Children: ";	// Has children?
	if (!children.empty())
	{
		std::cout << "True" << std::endl;
	}
	else
	{
		std::cout << "False" << std::endl;
	}

	std::cout << "\nPhysics:" << std::endl;
	std::cout << "Mass: " << mass << std::endl;
	std::cout << "Angular Displacement: " << getAngularDisplacement() << std::endl;
	std::cout << "Angular Applied Force Mag: " << angularForceApplied.getMag() << std::endl;
	std::cout << "Angular Normal Force Mag: " << angularForceNormal.getMag() << std::endl;
	std::cout << "Angular Net Force Mag: " << angularForceNet.getMag() << std::endl;
	std::cout << "Parallel Applied Force Mag: " << parallelForceApplied.getMag() << std::endl;
	std::cout << "Parallel Normal Force Mag: " << parallelForceNormal.getMag() << std::endl;
	std::cout << "Parallel Net Force Mag: " << parallelForceNet.getMag() << std::endl;
	std::cout << std::endl;
}


void Plant_Node::DBG_PrintRecursive()
{
	DBG_PrintSingle();

	for (Plant_Node * node : children)	// Recursive step
	{
		node->DBG_PrintRecursive();
	}
}

void Plant_Node::DBG_PrintTree()
{
	std::cout << "PRINTING PLANT_NODE TREE ==============" << std::endl;
	DBG_PrintRecursive();
	std::cout << "\nPrint complete.\n" << std::endl;
}



void Plant_Node::DBG_DrawInfo(SDL_Renderer * renderer)
{
	//SDL_SetRenderDrawColor(renderer, C_BLACK.r, C_BLACK.g, C_BLACK.b, C_BLACK.a);
	
	SDL_Point textBoxPos;
	textBoxPos.x = 1000;
	textBoxPos.y = 100;

	SDL_Rect currentTextBoundary;
	currentTextBoundary.x = textBoxPos.x;
	currentTextBoundary.y = textBoxPos.y;
	currentTextBoundary.w = 0;
	currentTextBoundary.h = 30;


	DBG_DrawTextLine(renderer, "Selected Node", &currentTextBoundary);
	DBG_DrawTextLine(renderer, "Mass: ", &currentTextBoundary);
}


void Plant_Node::DBG_DrawTextLine(SDL_Renderer * renderer, const char * text, SDL_Rect * boundary)
{
	// Get # of chars in text input
	int charCount = 0;
	while (text[charCount])
	{
		charCount++;
	}

	TTF_Font* font = TTF_OpenFont("cour.ttf", 50);
	SDL_Color textColor = { 255, 255, 255, 255 };
	SDL_Surface* textSurface = TTF_RenderText_Solid(font, text, textColor);
	SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

	boundary->w = charCount * 20;

	SDL_RenderCopy(renderer, textTexture, NULL, boundary);

	boundary->y += 30;
}