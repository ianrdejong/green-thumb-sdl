#pragma once

#include <vector>
#include "Vector2D.h"
#include "Colour_Constants.h"
#include "SDL.h"


/*
	*** IF YOU EXAMINE THE OUTPUTS OF THE UTIL FUNCTIONS THEY ARE NOT IMPLEMENTED PROPERLY. PLS DEBUG SOMETIME.
*/

// A tree of these nodes defines the structure and physics of a model plant.
class Plant_Node{

public:
	// CTORS & DTORS
	Plant_Node();
	Plant_Node(SDL_Point pos);
	~Plant_Node();


	// SETTERS & GETTERS
	void setPosition(const SDL_Point * point);
	SDL_Point getPosition();
	void setParent(Plant_Node * parent);


	// MAIN FUNCTIONS
	void updatePosition();	// Updates the position of node based on the position of its parent and its relative position
	void addChild();
	void tick(SDL_Renderer * renderer);	//	called every frame, updates shit
	void render(SDL_Renderer * renderer);
	Plant_Node * findNodeOnPoint(SDL_Point point);


	// PHYSICS FUNCTIONS
	void calculatePhysics(SDL_Renderer * renderer);	// Recursively apply physics forces onto all nodes
	void calculateAppliedForces(SDL_Renderer * renderer);
	void calculateNormalForces(SDL_Renderer * renderer);	// IT MAY BE BETTER NOT TO DO THESE RECURSIVELY
	void calculateNetForces(SDL_Renderer * renderer);
	double getAngularDisplacement();
	void applyPhysics();


private:
	SDL_Point position;
	Vector2D positionRelativeToParent;
	Vector2D originalPositionRelativeToParent;
	Plant_Node* parent;
	std::vector<Plant_Node*> children;

	Vector2D appliedForce;	// The total force applied to this node

	Vector2D angularForceApplied;
	Vector2D angularForceNormal;
	Vector2D angularForceNet;

	Vector2D parallelForceApplied;
	Vector2D parallelForceNormal;
	Vector2D parallelForceNet;

	float mass;

private:
	float const DEFAULT_MASS = 5;
	float const DEFAULT_DISTANCE = 100;
	float const MAX_ANGLE = 1;
	float const GRAVITATIONAL_CONSTANT = 9.8;

public:
	void DBG_DrawInfo(SDL_Renderer * renderer);
	void DBG_DrawTextLine(SDL_Renderer * renderer, const char * text, SDL_Rect * boundary);
	void DBG_PrintSingle();		// Prints the plant node
	void DBG_PrintTree();		// Print the entire tree. Wrapper function for DBG_PrintRecursive() that bookends print statements for clarity
	void DBG_PrintRecursive();	// Prints this and all child nodes
};