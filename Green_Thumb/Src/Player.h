#pragma once

#include "SDL.h"
#include "SDL_image.h"



class Player {

public:
	Player(int xpos, int ypos, int size, SDL_Texture* playerTexture);

	~Player();

	void render(SDL_Renderer* renderer);	// Render the player texture to the rectangle defined by dstrect

	void updatePosition();	// Update the dstrect based on xpos and ypos

	SDL_Point getPosition();
	SDL_Rect getHitBox();

	// Move the player...
	void moveUp();	
	void moveDown();
	void moveLeft();
	void moveRight();

private:
	int xpos, ypos;		// Player position
	int size;		// Width and height to draw the player texture
	int movementSpeed;	// Number of pixels in a single direction that the player can move in a frame
	SDL_Texture* texture;	// The player texture
	SDL_Rect srcrect;	// The area on the player texture to use
	SDL_Rect dstrect;	// The area on the renderer in which to draw the player
};