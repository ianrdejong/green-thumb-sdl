#pragma once

#include <vector>
#include "Plant.h"
#include "Plant_Node.h"
#include "Player.h"
#include "Colour_Constants.h"
#include "PhysicsObject.h"
#include "SDL.h"

class World{

public:
	World();
	~World();

	void render(SDL_Renderer* renderer);
	void generateWorld(SDL_Renderer* renderer);
	void update(SDL_Renderer * renderer);
	void updatePlayer(Player* player);
	void addPlantCell();
	void addPlant(int xpos, int ypos, SDL_Renderer* renderer);
	
	/*
	void DBG_PrintTree();
	void DBG_TreeTickSingle();
	void DBG_SelectNode(SDL_Point point);
	void DBG_PrintSelectedNode();
	*/

private:
	std::vector<Plant*> plants;
	PhysicsObject * physicsObject;
	PhysicsObject * selectedphysicsObject;
};