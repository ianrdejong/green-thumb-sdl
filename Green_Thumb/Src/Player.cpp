
#include "Player.h"

Player::Player(int xpos, int ypos, int size, SDL_Texture* playerTexture)
{
	this->xpos = xpos;
	this->ypos = ypos;
	this->texture = playerTexture;
	this->size = size;
	this->movementSpeed = 5;

	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = 72;
	srcrect.h = 120;

	dstrect.x = xpos;
	dstrect.y = ypos;
	dstrect.w = size;
	dstrect.h = size;
}


void Player::render(SDL_Renderer *renderer)
{
	SDL_RenderCopy(renderer, texture, &srcrect, &dstrect);
}

SDL_Point Player::getPosition()
{
	SDL_Point p;
	p.x = xpos;
	p.y = ypos;
	return p;
}

SDL_Rect Player::getHitBox()
{
	return dstrect;
}

void Player::updatePosition()
{
	dstrect.x = xpos;
	dstrect.y = ypos;
}

void Player::moveLeft()
{
	this->xpos -= movementSpeed;
}

void Player::moveRight()
{
	this->xpos += movementSpeed;
}

void Player::moveUp()
{
	this->ypos -= movementSpeed;
}

void Player::moveDown()
{
	this->ypos += movementSpeed;
}


Player::~Player()
{
	SDL_DestroyTexture(texture);
}