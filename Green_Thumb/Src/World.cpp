
#include <iostream>
#include "SDL_image.h"

#include "World.h"

World::World()
{
	plants = std::vector<Plant*>();
	physicsObject = new PhysicsObject();
	Vector2D pos;
	pos.setPosition({ 800, 800 });
	physicsObject->setPosition(pos);
}


World::~World()
{
	for (size_t i = 0; i < plants.size(); i++)
	{
		plants.at(i)->~Plant();
		plants.at(i) = nullptr;
	}
}



void World::update(SDL_Renderer * renderer)
{
	physicsObject->update();
}



void World::render(SDL_Renderer* renderer)
{
	SDL_SetRenderDrawColor(renderer, C_GRASS.r, C_GRASS.g, C_GRASS.b, C_GRASS.a);
	SDL_RenderClear(renderer);	// Clear the render buffer, draw background

	physicsObject->render(renderer);
	

	for (size_t i = 0; i < plants.size(); i++)
	{
		if (plants.at(i)->getIsAlive())	// If plant is alive
		{
			plants.at(i)->render(renderer);
		}
		else
		{							// If plant is dead
			plants.at(i)->~Plant();
			plants.at(i) = nullptr;
			plants.erase(plants.begin() + i);
		}
	}
}

void World::generateWorld(SDL_Renderer* renderer)
{
	SDL_Surface *tmpSurface = IMG_Load("../Assets/Bramble_2.png");
	if (tmpSurface == NULL)
	{
		std::cout << "Error loading image" << std::endl;
	}

	SDL_FreeSurface(tmpSurface);
}


void World::updatePlayer(Player* player)
{
	for (size_t i = 0; i < plants.size(); i++)
	{
		if (plants.at(i)->checkPlayerIntersection(player))
		{
			std::cout << "Touching!" << std::endl;
		}
	}
}


void World::addPlantCell()
{
	
}


void World::addPlant(int xpos, int ypos, SDL_Renderer* renderer)
{
	SDL_Surface *tmpSurface = IMG_Load("../Assets/Bramble_2.png");
	if (tmpSurface == NULL)
	{
		std::cout << "Error loading image" << std::endl;
	}
	plants.push_back(new Plant(xpos, ypos, 40, 40, SDL_CreateTextureFromSurface(renderer, tmpSurface), 32, 32));
	SDL_FreeSurface(tmpSurface);
}



/*
void World::DBG_PrintTree()
{
	rootNode->DBG_PrintTree();
}

void World::DBG_TreeTickSingle()
{
	rootNode->applyPhysics();
}


void World::DBG_SelectNode(SDL_Point p)
{
	selectedNode = rootNode->findNodeOnPoint(p);
}


void World::DBG_PrintSelectedNode()
{
	if (selectedNode != nullptr)
	{
		selectedNode->DBG_PrintSingle();
	}
}

*/