
#ifndef COLOUR_CONSTANTS_H
#define COLOUR_CONSTANTS_H

#include "SDL.h"

// ****** THIS SHOULD BE A STATIC CLASS OR WHATEVER THAT IS IN c++

const SDL_Color C_GRASS = { 40, 83, 30, 255 };
const SDL_Color C_PLANTS = { 28, 73, 13, 255 };
const SDL_Color C_PLANTS_DARK = { 10, 40, 5, 255 };

/* Basic */
const SDL_Color C_BLACK = { 0, 0, 0, 255 };
const SDL_Color C_RED = { 150, 16, 16 };
const SDL_Color C_DARK_RED = { 100, 0, 0 };
const SDL_Color C_PINK = { 255, 165, 165 };
const SDL_Color C_BLUE = { 16, 65, 150 };
const SDL_Color C_LIGHT_BLUE = { 43, 140, 219 };

#endif /* COLOUR_CONSTANTS_H */