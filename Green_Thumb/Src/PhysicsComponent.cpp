

#include "PhysicsComponent.h"


PhysicsComponent::PhysicsComponent()
{
	mass = 1;
	position = Vector2D();
	velocity = Vector2D();
	acceleration = Vector2D();
}


PhysicsComponent::~PhysicsComponent()
{

}


void PhysicsComponent::update()
{
	velocity.setX(velocity.getX() + acceleration.getX());
	velocity.setY(velocity.getY() + acceleration.getY());
	position.setX(position.getX() + velocity.getX());
	position.setY(position.getY() + velocity.getY());
}


void PhysicsComponent::applyConstantForce(Vector2D force)
{
	acceleration += force / mass;
}


void PhysicsComponent::applyImpulse(Vector2D force)
{
	Vector2D impulseAccel = force / mass;
	velocity.setX(velocity.getX() + impulseAccel.getX());
	velocity.setY(velocity.getY() + impulseAccel.getY());
}





/* =========== SETTERS & GETTERS =========== */

void PhysicsComponent::setMass(unsigned int mass)
{
	this->mass = mass;
}


void PhysicsComponent::setPosition(Vector2D position)
{
	this->position = position;
}


void PhysicsComponent::setVelocity(Vector2D velocity)
{
	this->velocity = velocity;
}


void PhysicsComponent::setAcceleration(Vector2D acceleration)
{
	this->acceleration = acceleration;
}


unsigned int PhysicsComponent::getMass() const
{
	return this->mass;
}

Vector2D PhysicsComponent::getPosition() const
{
	return this->position;
}


Vector2D PhysicsComponent::getVelocity() const
{
	return this->velocity;
}


Vector2D PhysicsComponent::getAcceleration() const
{
	return this->acceleration;
}